# Guidelines for licensing your work

There is no neat procedure telling how to license your work, as there are many implications that are project-specific. 
A proposed policy for the ABP group was discussed on 20 June 2022 at the ABP Computing Meeting - see [indico](https://indico.cern.ch/event/1158301/):

1. Inform supervisor about software produced before distributing it.
2. For new projects (due diligence):
   1. Add copyright notice in each file.
   2. If software is to be distributed publicly, add a license (for instance consider GPL first because it can be dual licensed later if all authors agree).
   3. If software has potential and rely on outside collaborators establish with KT and hierarchy a license.
   4. Keep updated author list with time and affiliation.
   5. Keep list of essential components used, their license and check license compatibilities.
   6. Consider use SPDX identifier.
3. For existing large project:
   1. Attempt to catch-up with the policy above knowing that:
   2. In case of shared ownership the license cannot be changed without written agreement.
   3. In case of license infringement of components either:
      1. Replace component or
      2. Change license if possible to do so
   4. Good luck, experience has shown that catching-up is a very time consuming, costly, unpleasant work.

> Note: KT can be consulted for each of these steps.

