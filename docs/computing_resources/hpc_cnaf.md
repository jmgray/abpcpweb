# High Performance Computing clusters at INFN-CNAF

An HPC cluster dedicated to CERN studies is available at INFN-CNAF (Bologna, Italy) allowing for MPI applications across multiple nodes.
The cluster presently features a total of about 800 CPU-cores.

## Access
Access to the cluster can be requested by sending an email to abp.computing[AT]cern.ch including the following information:

 - The filled in <a href="https://www.cnaf.infn.it/wp-content/uploads/2022/03/Request-CNAF-account.pdf" target="_blank">form</a>. Please add Daniele Cesini as contact person and "Access to the hpc_acc Cluster to run High Energy Physics simulation codes" as the reason.

## Usage

 - Users should connect via ssh to **bastion.cnaf.infn.it** and from there access the cluster front-end called **ui-hpc2.cr.cnaf.infn.it**.
 - The LSF system is installed on the cluster to manage job submissions. Jobs should be submitted to the ”hpc-acc” queue.
 - If you have inquiries about running your jobs on this cluster or need technical support, please write an email to <a href="mailto:hpc-support@listsNOSPAMPLEASE.cnaf.infn.it">hpc-support@listsNOSPAMPLEASE.cnaf.infn.it</a>
 - A node called **hpc-201-11-35** is equipped with four V100 **GPUs**, and can be used interactively.

 To install `sixtrack` on **hpc-201-11-35** you can source

```
scl enable devtoolset-7 bash
git clone https://github.com/SixTrack/SixTrack.git
cd SixTrack
sed -i 's/cmake /cmake3 /g' cmake_six
./cmake_six
```

 A possible trick to connect to the CNAF server is to configure your `~.ssh/config` (on your local machine, I tested it on my OSX and on UBUNTU) with

```
Host bastion
 HostName bastion.cnaf.infn.it
 User your_user_name
 ForwardX11 yes
 ForwardX11Trusted yes

Host b001
 User your_user_name
 HostName hpc-201-11-01-a
 ProxyJump bastion
 ForwardX11 yes
 ForwardX11Trusted yes
 LocalForward your_port_number_1 localhost:your_port_number_1

Host bgpu
 User your_user_name
 HostName hpc-201-11-35.cr.cnaf.infn.it
 ProxyJump bastion
 ForwardX11 yes
 ForwardX11Trusted yes
 LocalForward your_port_number_2 localhost:your_port_number_2
``` 

where you have to replace `your_user_name`, `your_port_number_1` and `your_port_number_2`.
The `your_port_number_1` and `your_port_number_2` can be guessed (e.g., typically integer >8000, if someone is already using the port will be available for you).

 Then you can launch (if installed in your miniconda) `jupyter lab` on `b001` or `bgpu` (first you need to connect via `ssh b001` or `ssh bgpu`) 

```
jupyter lab --no-browser --port your_port_number
```

where `your_port_number` is `your_port_number_1` or `your_port_number_2` depending if you are connected to `b001` or `bgpu`, respectively. If you open your jupyter server in a `tmux`, you can start working on your desktop at the office and continue when you TW from home.  

## Other links
__Relevant links__ to obtain further information on this cluster are:

 <ul><li> Presentation by Antonio Falabella at <a href="https://indico.cern.ch/event/647418/" target="_blank">ABP-CWG meeting on 20 July, 2017</a> 
</li> <li> Presentation by Annalisa Romano on how to submit jobs at <a href="https://indico.cern.ch/event/647301/" target="_blank">ABP-CWG meeting on 4 July 2017</a>
</li> <li> Note on outcome of the acceptance test to proceed to the second phase of the cluster procurement and installation: <em>Performance of space charge simulations using High Performance Computing (HPC) cluster</em> <a href="https://cds.cern.ch/record/2276464" target="_blank">CERN-ACC-NOTE-2017-0048</a>
</li></ul>

 
## Support

* Please note that Antonio Falabella ([antonio.falabella@cnafNOSPAMPLEASE.infn.it](mailto:antonio.falabella@cnafNOSPAMPLEASE.infn.it)) is available to assist you, should you encounter any problem in connecting or running your jobs in the present configuration.

 
